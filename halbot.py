#!/usr/bin/env python3

# Import library for interactions with Mastodon API
from mastodon import Mastodon
# Import StreamListener from Mastodon Streaming API
from mastodon.streaming import StreamListener

# Import library for running a ChatterBot
from chatterbot import ChatBot

# Import library from HTML manipulation
from bs4 import BeautifulSoup


# creates login credentials (only needs to be run one time)
def create_creds(instance):
	Mastodon.create_app(
		'halbot',
		api_base_url='https://' + instance,
		to_file='halbot_clientcred.secret'
	)
	mastodon = Mastodon(
		client_id='halbot_clientcred.secret',
		api_base_url='https://' + instance
	)
	# !!! Here is where you need to fill in the username + password in clear-text !!!
	mastodon.log_in(
		'user@email.com',
		'supersecretpassword',
		to_file='halbot_usercred.secret'
	)


# Parses the actual content from the status notification
def parse_content(status_content):
	# Remove the HTML tags from status_content
	status = BeautifulSoup(status_content, "lxml").text
	print("Received this toot: " + status)
	# Remove the first word from string
	# TODO Remove usernames instead of the first word
	cleartext = status.split(' ', 1)[1]
	return cleartext


# Creating a SubClass of Mastodon.StreamListener to define behavior on Notification
class Listener(StreamListener):
	def __init__(self, lstnr_apiconn):
		self.api_conn = lstnr_apiconn
	
	def on_notification(self, notification):
		print('The StreamListener received a new notification...')
		# Uncomment line below to troubleshoot Streaming API notification
		#print(notification)
		
		# Parse the user name from the notification
		fedi_user = notification["account"]["acct"]
		# Parse the content from the notification
		user_response = parse_content(notification["status"]["content"])
		print("Processing this Response: " + user_response)
		# Parse the Reply-To ID from the notification
		reply_to_id = notification["status"]["id"]
		
		# Check whether we already have a Bot instance for this Fediverse account
		if fedi_user in open_conversations:
			bot_instance = open_conversations[fedi_user]
			# Generate HAL's response
			reply_message = '@' + str(fedi_user) + ' ' + str(bot_instance.get_response(user_response))
		else:
			# Create a new Bot instance for this conversation
			bot_instance = ChatBot(
				"HAL9000", 
				storage_adapter='chatterbot.storage.SQLStorageAdapter',
				logic_adapters=[
					'chatterbot.logic.MathematicalEvaluation',
					'chatterbot.logic.BestMatch'
				],
				database_uri='sqlite:///db.sqlite3'
			)
			# Add the new Bot instance to the dictionary of open conversations
			open_conversations[fedi_user] = bot_instance
			print("bot instance addedd")
			# Generate HAL's response
			reply_message = '@' + str(fedi_user) + ' ' + str(bot_instance.get_response(user_response))
		
		# Send Bot's reply into Mastodon API
		self.api_conn.status_post(reply_message, in_reply_to_id = reply_to_id)


# main loop
if __name__ == '__main__':
	# Put in your instance name here:
	mastodon_instance = 'ioc.exchange'
	#create_creds(mastodon_instance)
	
	# Dictionary to keep track of open conversations
	open_conversations = {}
	
	# connect to Instance API
	mstdn = Mastodon(
		access_token='halbot_usercred.secret',
		api_base_url='https://' + mastodon_instance
	)
	# Spawn StreamListener, which is the Object interacting with Fediverse
	listener = Listener(mstdn)
	# Open the Stream and connect the StreamListener to it
	stream = mstdn.stream_user(listener)
