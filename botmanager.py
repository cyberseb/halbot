#!/usr/bin/env python3

# Import library for running a ChatterBot
from chatterbot import ChatBot

# Import library for ChatterBot Trainers
from chatterbot.trainers import ListTrainer
from chatterbot.trainers import ChatterBotCorpusTrainer
from chatterbot.trainers import UbuntuCorpusTrainer


def train_bot(chatbot):
	# Free-style Training
	conversation = [
		"Hello", 
		"Hi there!", 
		"How are you doing?", 
		"I'm doing great.", 
		"That is good to hear", 
		"Thank you.", 
		"You're welcome."
	]
	trainer = ListTrainer(chatbot)
	trainer.train(conversation)
	# English Training
	trainer2 = ChatterBotCorpusTrainer(chatbot)
	trainer2.train('chatterbot.corpus.english')


# main loop
if __name__ == '__main__':
	# Create our chat bot
	bot_instance = ChatBot(
		"HAL9000", 
		storage_adapter='chatterbot.storage.SQLStorageAdapter',
		logic_adapters=[
			'chatterbot.logic.MathematicalEvaluation',
			'chatterbot.logic.BestMatch'
		],
		database_uri='sqlite:///db.sqlite3'
	)
	
	# Train the chat bot
	train_bot(bot_instance)
	
	while True:
		try:
			bot_input = bot_instance.get_response(input())
			print(bot_input)

		except(KeyboardInterrupt, EOFError, SystemExit):
			break
